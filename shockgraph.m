A = dlmread( "shockdata.csv" ),
graphdata = {A(2, :), A(3:7, :), A(8:10, :), A(11:15,:), A(16:18, :)}
figure
hold on
for i = 1:length(graphdata),
    a = [[0, 1]; [graphdata{i}(:,3), 1- graphdata{i}(:, 6) ./ graphdata{i}(:, 5) ]];
    plot(a(:,1), a(:, 2));
endfor
title("Fraction of successful drops")
xlabel("Shock energy (J)")
ylabel("Successes / drops")
legend("1 connection", "3 connections", "5 connections", "8 connections", "12 connections");
