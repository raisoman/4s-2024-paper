
\documentclass[pdflatex,sn-basic]{sn-jnl}% Basic Springer Nature Reference Style/Chemistry Reference Style
% \usepackage{graphicx}%
% \usepackage{multirow}%
\usepackage{amsmath,amssymb,amsfonts}%
% \usepackage{amsthm}%
% \usepackage{mathrsfs}%
% \usepackage[title]{appendix}%
% \usepackage{xcolor}%
% \usepackage{textcomp}%
% \usepackage{manyfoot}%
% \usepackage{booktabs}%
% \usepackage{algorithm}%
% \usepackage{algorithmicx}%
% \usepackage{algpseudocode}%
% \usepackage{listings}%
\usepackage{caption}
\usepackage{subcaption}
\usepackage{epstopdf}

\begin{document}
\title[Article Title]{S\textsuperscript{4} - A Synchrokinetic Standard Satellite Structure}

\author*[1]{\fnm{Nicolas} \sur{Guilbert}}\email{nicolas@ange.dk}
\author[2]{\fnm{Sacha} \sur{Guilbert}}\email{sachaguilbert@gmail.com}

\affil*[1]{\orgname{Ange Optimization}}

\affil[2]{\orgname{Technical University of Denmark}}


\abstract{
The near future will see a drastic increase in the total mass and number of assets in orbit, leading to a maturation of the industry into mass production, in turn requiring standardization of the building blocks of space constructions, and more broadly for ISAM in general (In-orbit servicing, assembly and manufacturing). A newly invented assembly method, \emph{synchrokinesis}, could potentially be pivotal in the development of the forthcoming in-orbit industry, as it drastically simplifies the work of the involved robots: the assembly and disassembly is carried out by simple, synchronous, linear translation of the components as opposed to the current practice based on traditional fastening techniques such as screws, soldering etc. The approach offers an opportunity to promote standardization at the level of both mechanical and electronic connectors.
}

\keywords{synchrokinesis, autonomous robotic assembly, in-space assembly, ISAM}

\maketitle

\section{Introduction}\label{introduction}

The last decade has seen a revolution in the area of space transportation and it would appear that it is only the beginning: the price per kilogram to orbit has already plummeted to USD 5k, mainly due to SpaceX’s introduction of reusable Falcon 9 rockets. The ambition is to reduce it even further by orders of magnitude, aspiring, in the long term, to reach USD 10 / kg using the upcoming Starship system. Given the current development pace, this could lead to opportunities that were previously unimaginable within a few years, if not months.

The number and total mass of systems in orbit is bound to increase drastically, irrespectively of where that price exactly stabilizes, making it relevant to consider inter-system optimizations. How these interactions will materialize is open for speculation: will it be refueling missions or other means to increase altitude? service and repair of space crafts? reuse, reconfigurations and consolidations of swarms? In any case, a high degree of automation will be required and assembly/disassembly will be key.

Enters \emph{synchrokinesis} \cite{guilbert-patent-2023}, an assembly technique devised for robotized execution in weightlessness; the first fastener-free, frictionless, self-interlocking and reversible assembly technique, relying on a precise, timely and simultaneous translation of the parts. Very simple and well suited for a robot, less so for human hands. And well suited for standardization.

It could be a key differentiating technology when it comes to intra-fleet asset optimization: the alternative would be to have maintenance robots carrying out assembly/disassembly using complex screwdriver-like mechanisms and robotics arms in a process that would be difficult to standardize, leading to a plethora of competing incompatible systems and possibly unsurmountable complexity. Another alternative would be to have astronauts carrying out the tasks with manual tools, which would not work at scale.

This will facilitate a large-scale production of satellites on the ground, an intermediate step before producing and maintaining them in orbit.

The purpose of this article is to explain synchrokinesis in its current early stage of advancement and to sketch the the scenarios for potential uses in the space industry, including considerations on standardization.

The structure of the paper is as follows: First, summarize the state-of-the art of ISAM (In-orbit servicing, assembly and manufacturing) in Section \ref{isam}. In Section \ref{principles}, we then explain the principles of synchrokinesis and illustrate these with experimental results of various stress tests in Section \ref{experiments}.  In Section \ref{isam-applications} we consider how synchrokinesis applies to ISAM. Also, the inherent modularity of the concept has led to some considerations on standardization, which we elaborate on in Section \ref{standardization} and present S\textsuperscript{4} - the Synchrokinetic Standard Satellite Structure in Section \ref{s4}. We conclude the paper in Section \ref{conclusion}.


\section{In-orbit Servicing, Assembly and Manufacturing}\label{isam}

In-Space Servicing, Assembly and Manufacturing (ISAM) \cite{isam-state-of-play} could become the most significant industry within decades, the future of spaceflight promising increasingly ambitious missions to support civil, national security, and commercial space endeavors. However, achieving these missions won't be feasible through a single launch of an integrated, fully functional system. For instance, future science and human exploration missions will require payloads larger than any foreseeable launch vehicle fairing can accommodate. National security missions will demand persistent assets that are both mobile and resilient, while commercial space missions will need cost-effective ways to update technology while in orbit.

ISAM offers a transformative approach, vastly enhancing the performance, availability, and lifespan of space systems compared to the traditional paradigm of launching assets without the possibility of servicing. ISAM capabilities create an ecosystem that shifts the space operations paradigm, laying the groundwork for sustainable exploration and amplifying other capabilities such as space logistics, power generation, and reusability.
Past achievements in ISAM, such as the International Space Station (ISS) operations and maintenance and servicing missions to the Hubble Space Telescope (HST)  illustrate the significant operational feats made possible by ISAM capabilities. Ongoing and upcoming flight demonstrations are further advancing areas crucial for the next generation of orbital missions, including large missions such as the Lunar Gateway \cite{lunar-gateway} and Artemis Base Camp, and even beyond terrestrial orbits.

\section{The principles of synchrokinesis}\label{principles}

Synchrokinesis is a concept partly inspired by Japanese wood joinery techniques and Chinese puzzles, partly enabled by robotics and allows to assemble polyhedrical structures without fasteners. The project that sparked the idea was to build a satellite aimed at being as easy as possible to assemble for a robot, with the purpose of lowering the hurdle of going from prototype to production at scale, or in other words adopt an "automation first" approach.

The assembly process is based on a simple principle of simultaneity, i.e. that all parts need to be translated at the same time to fit together. Conversely, disassembly requires the opposite simultaneous movement, which cannot happen randomly. Traditional assembly methods fundamentally rely on an irreversible process to function, for instance a nut and bolt rely on overcoming a barrier of friction, a nail deforms the material it is inserted into, soldering relies on the phase shifts of the solder, glueing on the glue's chemical hardening process etc. Synchrokinesis on the other hand relies on purely geometrical constraints, requiring only an infinitesimal amount of energy to frictionlessly slide together, see an example in Figure \ref{fig:synchrokinetic-sequence}.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{cube-sequence-0.jpg}
  \caption{}
  \label{fig:sub-first}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include second image
  \includegraphics[width=.8\linewidth]{cube-sequence-1.jpg}
   \caption{}
  \label{fig:sub-second}
\end{subfigure}

% \newline

\begin{subfigure}{.5\textwidth}
  \centering
  % include third image
  \includegraphics[width=.8\linewidth]{cube-sequence-2.jpg}
  \caption{}
  \label{fig:sub-third}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include fourth image
  \includegraphics[width=.8\linewidth]{cube-sequence-3.jpg}
  \caption{}
  \label{fig:sub-fourth}
\end{subfigure}
  \caption{Simultaneous translations of six parts during synchrokinetic assembly.}
\label{fig:synchrokinetic-sequence}
\end{figure}

The behavior of the synchrokinetic ensemble is best understood probabilistically and can be modeled statistically by Brownian motion: at each point in time, each of the parts undergoes a random movement in any direction, due to either thermal activity, vibrations, shocks etc. If that movement happens to be along the single axis of translation of each part allowed by the geometry, the axis of synchrokinesis, the part will displace itself relatively to the ensemble, if not, it will remain stationary since it will be constrained by the other parts.  How closely aligned the random movement and axis of synchrokinesis need to be for displacement to occur depends on the tolerance of the parts. In practice it will also depend on the friction between the parts.

In the most simple case - an ensemble consisting of two parts, displacement will occur if the component of the random impulse parallel to the axis of synchrokinesis is able to overcome the friction, taking into consideration that the friction increases with the orthogonal component of the impulse. In the case of three or more parts however, at each point in time, a displacement of each part can only happen if the random movement is aligned with its axis of synchrokinesis, a situation that needs to occur for all the parts in the ensemble simultaneously. The probability of this synchronized event happening drops exponentially with the number of parts in the ensemble and the structure is in consequence stable and robust towards random noise.

In real-world situations, biases on the random motion exist. Impacts on an object, for instance when it falls on the floor, or air resistance, will typically happen from the outside, creating an inward bias on the Brownian motion, stabilizing and strengthening the ensemble. So will gravity since it works as an overall compressing force. On the other hand, containment of a gas under pressure would create an outward bias, and so would centrifugal forces. Stability of the ensemble is dependent on the sum of these biases, for instance it could contain a gas if its parts are heavy enough for gravity to keep it together.

\subsection{A Brownian model}

A thorough mathematical model of a synchrokinetic ensemble could rapidly become very complex: one avenue to pursue would be to consider the random movement of each part separately and consider the compound probability of them moving apart, i.e. the probability of distribution of the product of the variables. This naive approach becomes difficult already for a number of connections between elements of $N_c=3$ and prohibitively complex for $N_c>3$.

We will in consequence consider a simpler model making use of the fact that the synchrokinetic ensemble only has one degree of freedom. We describe its state $P(t)$ probabilistically by the following Brownian motion model based on a Gaussian random walk:
$$
P(t) \propto e^{-\frac{1}{T}(\frac{\phi}{\tau})^{N_c} t^2}
$$
where $P(t)$ is a probability distribution describing the displacement along say the first element's axis of synchrokinesis after a time $t$, $\tau \in ]0, 1]$ describes the mechanical tolerance between parts, $\phi > 0$ the friction, $T$ the temperature and $N_c$ the number of connections between elements. The tolerance $\tau$ is a number between 0 and 1, indicating the fraction of possible directions a part can move with respect to another: an infinitesimally small tolerance would in theory mean that the parts can only move in one direction relative to each other and at the other extreme of the interval, a value of 1 would mean that the parts are not limiting each other. In practice, a value of 0.01 would be low, and a value of 0.2 would be high. The friction coefficient $\phi$ linearly approximates the behavior that in each step of the random walk, the probability of a displacement occurring along an axis of synchrokinesis between two parts decreases with friction.

The exponentiation with $N_c$ in $(\frac{\phi}{\tau})^{N_c}$ is the key to synchrokinesis: the higher the number of connected, mutually constraining components, the lower the probability that they will be displaced in each step of the random walk, a probability that drops exponentially with $N_c$. Note that the friction coefficient is included under the $N_c$-exponent: this is due to the fact that the parts when moving randomly will slightly twist, causing an increase in the pressure between the parts, increasing the friction. The more constrained one part is by the other parts, the more it will be affected by this increase in friction.


\section{Applications and implications of synchrokinesis to ISAM}\label{isam-applications}

Synchrokinetic assembly is potentially relevant for a number of areas within ISAM:

\emph{Robotic Manipulation}: Robotic manipulators, deployed on missions ranging from Mars surface exploration to ISS operations, are evolving to enhance autonomy, cut costs, and boost space robotics capabilities. Synchrokinesis is in its initial form intended to be carried out using robotic linear actuators, but electromagnetic actuation should be researched as a possibility \cite{emff-mit-2005}, since it would enable completely autonomous assembly/disassembly, free of any mechanical complexity.

\emph{Planned Repair, Upgrade, Maintenance, and Installation}: Planned servicing forms the core of an ISAM ecosystem, with spacecraft and client objects designed to work seamlessly together. Modular interfaces facilitate mechanical, fluid, power, data, and thermal connections for various spacecraft types. The \emph{seamless} integration will require an ongoing standardization effort, preferably starting as early as possible. This will be the topic of Section \ref{standardization}.

\emph{Refueling and Fluid Transfer}: Demonstrations of storable fluid transfer, including on the ISS, are paving the way for commercial refueling services, especially for storable fluids. Future tests currently aim to validate large-scale cryogenic fluid transfer in space \cite{elon-musk-update-on-starship-2024-04}. However, one possible alternative architecture would be to consider fuel as a part, i.e. refueling would consist of replacing the entire fuel tank and be carried out in a two-step process consisting of an disassembly followed by a reassembly step for replacing the fuel component.

\emph{Structural Manufacturing and Assembly}: Advancements in structural manufacturing and assembly emphasize autonomous, robotic methods over astronaut assembly, broadening capabilities for constructing spacecraft components or subsystems. \cite{hammond-2023}

\emph{Recycling, Reuse, \& Repurposing}: Technologies for recycling, reuse, and repurposing spacecraft materials aim to integrate them into a sustained space presence. Future efforts focus on expanding reusable materials, tailoring their performance, and understanding mission implications. This would drastically reduce the environmental impact of the space industry as a whole. Standardization of components and connectors would be key to maximizing recycling, reusing and repurposing.

\emph{Parts and Goods Manufacturing}: Initial focus on 3D printed plastics expands to include metals, electronics, and in-situ regolith-based materials, broadening production capabilities in space. Once base components have been manufactured, the next step would be to assemble them into larger structures.\cite{nasa-lunar-surface-2022}

\emph{Surface Construction}: Advancements in surface construction encompass horizontal and vertical infrastructure needs, from landing pads to habitation structures, addressing requirements for excavation, construction, and outfitting on planetary surfaces. The Tall Lunar Tower \cite{tall-lunar-tower} is an example of such a construction to be built through completely robotized assembly.


\section{Considerations on standardization}\label{standardization}
The importance of standardization is easily overlooked, particularly in the early phases of innovation when the focus tends to be on creativity and experimentation. Yet, without standardized practices and protocols, the scalability, interoperability, and sustainability of innovations are at risk. Also, synchrokinetic assembly would seem to lend itself particularly well to standardization: it has a trait of universality and can connect "anything to anything" just like nuts and bolts. It would be a shame to miss out on that opportunity. However, premature standardization could also be detrimental by stifling innovation, so we will here limit ourselves to present the following preliminary considerations:

In a context of synchrokinesis, we foresee there be two mutually dependent areas to standardize: the mechanical and the electronic connectors. The reason why they depend on each other is that the electronic connectors are inherently mechanical as well, meaning that they would in any case require a specification of their physical geometry. Conversely, most satellite components can be expected to be electrified to be useful, be it to provide or receive data and/or power. 

In consequence it makes sense to specify both at the same time:

\begin{itemize}
\item Mixed symmetric connectors: The electronic interface would benefit from being genderless to maximize interoperability, imposing an antisymmetric physical geometry and mixed symmetric electronic bus configuration. Specifically the mixed symmetry is composed by symmetric connectors (e.g. power: VCC with VCC and ground with ground), and antisymmetric connections, e.g. serial Tx goes to Rx and vice versa.
\item Choice of the cube as basic volumetric pattern: For larger structures involving an arbitrary number of parts, it would be ideal to operate with a single type of base component. This excludes all other candidates than the cube, since it is the only Platonic solid that tesselates three-dimensionally. In consequence, other choices of base pattern would either cause voids in the structure, or require multiple elementary building block types.
\end{itemize}


\section{S\textsuperscript{4} - a technology demonstrator}\label{s4}
S\textsuperscript{4} - the Synchrokinetic Standard Satellite Structure, see Figure \ref{fig:s4pla} is a first attempt at a design satisfying the principles listed in Section \ref{standardization}. It is a structure geometrically complying with the CubeSat specification, being a cube with faces of 10cm x 10 cm. The prototype shown has been 3D-printed in polycarbonate using FDMs. The connectors are pads inclined 45° as seen in Figure \ref{fig:s4plaA}, with each of the faces' edges featuring a male and a female connector positioned antisymmetrically, enforcing a single degree of freedom in the movement between two parts.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{frame45.jpg}
    \caption{Antisymmetric, genderless (male and female) physical pad configurations. 45° inclination of the pads.}
    \label{fig:s4plaA}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{s4-PLA.jpg}
    \caption{A single face type is needed in this configuration,for all six faces. 3D printed in polycarbonate using FDM.}
\end{subfigure}
  \caption{Prototype of S\textsuperscript{4} structure}
\label{fig:s4pla}
\end{figure}

\section{Experiments}\label{experiments}

\subsection{Vibration table}
The vibration table illustrates the tendency of a synchrokinetic structure to consolidate itself. The structure starts out being maximally stretched apart and rapidly converges to its "condensed" state, see Figure \ref{fig:vibrationtable}. What happens is that the force of gravity is causing an inward bias on the Brownian movement and the vibrations then catalyze the assembly by overcoming the friction between the components. The structure used here is a low-friction structure, where the connections are conceived in such a way that they have two degrees of freedom rather than one, as described for S\textsuperscript{4} in Section \ref{s4}.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{rystebord-1.jpg}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{rystebord-2.jpg}
\end{subfigure}
\newline
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{rystebord-3.jpg}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{rystebord-4.jpg}
\end{subfigure}
  \caption{Synchrokinetic structure self-consolidating on vibration rig. The process lasts 1.8s.}
\label{fig:vibrationtable}
\end{figure}

\subsection{Acoustic actuation}
In continuation of the vibration table test, we investigate if the structure itself could generate the vibrations needed to overcome the friction by placing a buzzer on one of the faces. This test was not conclusive, as the buzzer's effect was insufficient. Either vibration generating mechanism should be used, or each face should have it's own, possibly being synchronized such as to maximimize their combined effect, and possibly making use of the structure's resonance.

\subsection{Shock testing}
At some point the vibrations could be expected to cause it to break apart, i.e. when the shocks are above a certain threshold. In order to investigate this we carried out a number of drop tests, varying the drop height and the number of parts in the assembly. The results are shown in Figure \ref{fig:shockgraphs}, where the fraction of successful (non-breaking) drops are plotted vs. the energy of the drop (mass $\times$ drop height $\times g$). The mass of each face was 20g and the drop height varied from 10 cm to 220 cm. The number of drops were limited above 200J, as the plastic of the structure was beginning to break.

The number of connections between components was related to the number of components as follows:
\begin{center}
\begin{tabular}{ | c | c | }
 \hline
 Number of components & Number of connections  \\
  \hline
 2 & 1 \\
 3 & 3 \\
 4 & 5 \\
 5 & 8 \\
 6 & 12 \\
  \hline
\end{tabular}
\end{center}


\begin{figure}
  \centering
  \includegraphics[width=.8\linewidth]{shockgraphs.eps}
  \caption{Resilience to shocks depending on number of interconnected components}
  \label{fig:shockgraphs}
\end{figure}


\section{Conclusion}\label{conclusion}

The purpose of this article was to introduce the concept of synchrokinesis and describe how it can be applied in contexts of assembly and disassembly of structures in weightlessness.

We have seen how synchrokinesis relies on the degrees of freedom of the overall structure being reduced to one, allowing for a simple model to describe its robustness. In particular the strength of the assembled construction increases with the number of components and more specifically, the effect of friction 
seems to increase combinatorically with the number of connections between elements.

The technique is applicable to most areas within In-orbit servicing, assembly and manufacturing (ISAM), either directly, or by slightly adapting the task to become an assembly operation (e.g. refueling).

We have argued that the "universal" aspect of the technique would justify early considerations on standardization, both regarding the geometry of the mechanic connectors, but also the electronic bus interfaces.

We finally experimentally investigated some of its mechanical properties, for instance its bias towards reassembly when submitted to gravity and its resilience to shocks and presented a product prototype of synchrokoinetic standard satellite structure - S\textsuperscript{4}.

\bibliography{references}
\bibliographystyle{plain}
 
\end{document}
